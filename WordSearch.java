import java.io.FileReader;
import java.io.BufferedReader;
import java.io.IOException;

public class WordSearch {

    private static int rows;
    private static int cols;
    private static char[][] maze;
    
    public static void main(String[] args) {
        String filename = "inputFile.txt";
        readInputFile(filename);
    }

    private static void readInputFile(String filename) {
        try (BufferedReader reader = new BufferedReader(new FileReader(filename))) {
            String dimension = reader.readLine().trim();
            String[] dimensions = dimension.split("x");
            rows = Integer.parseInt(dimensions[0]);
            cols = Integer.parseInt(dimensions[1]);

            maze = new char[rows][cols];
            for (int i = 0; i < rows; i++) {
                String[] rowChars = reader.readLine().trim().split(" ");
                for (int j = 0; j < cols; j++) {
                    maze[i][j] = rowChars[j].charAt(0);
                }
            }

            String wordLine;
            while ((wordLine = reader.readLine()) != null) {
                String word = wordLine.trim();
                String result = searchWord(word);
                if (result != null) {
                    System.out.println(result);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static String searchWord(String word) {
        int[] rowDirectionArray = {0, 0, 1, -1, 1, 1, -1, -1};
        int[] columnDirectionArray = {1, -1, 0, 0, 1, -1, 1, -1};

        for (int r = 0; r < rows; r++) {
            for (int c = 0; c < cols; c++) {
                for (int i = 0; i < 8; i++) {
                    int currentRow = r;
                    int currentCol = c;
                    boolean wordFound = true;
                    for (char ch : word.toCharArray()) {
                        if (!(currentRow >= 0 && currentRow < rows && currentCol >= 0 && currentCol < cols && maze[currentRow][currentCol] == ch)) {
                            wordFound = false;
                            break;
                        }
                        currentRow += rowDirectionArray[i];
                        currentCol += columnDirectionArray[i];
                    }
                    if (wordFound) {
                        return String.format("%s %d:%d %d:%d", word, r, c, currentRow - rowDirectionArray[i], currentCol - columnDirectionArray[i]);

                    }
                }
            }
        }
        return null;
    }
}